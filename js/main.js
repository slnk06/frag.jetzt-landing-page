/* JavaScript zum Beitreten einer Sitzung von frag.jetzt*/

/*Beitreten über eine ENTER Eingabe*/
document.querySelector("#sessionID").addEventListener("keydown", e => {
    const value = document.querySelector("#sessionID").value;
    if (e.keyCode == 13) visitSession(value);
});

/*Beitreten über klicken des "Sitzung Beitrenten" Knopfes*/
document.querySelector("#joinButton").addEventListener("click", function () {
    const value = document.querySelector("#sessionID").value;
    visitSession(value);
});
/*Weiterleitung zu der frag.jetzt Sitzung in einem neuen Tab*/
function visitSession(sessionID) {
    if (isValid(sessionID)) {
        window.open("https://frag.jetzt/participant/room/" +
            sessionID, "_blank");
    }
}
/*Überprüfung ob es sich wirklich um einen validen Sitzungs-Code handelt*/
function isValid(sessionID) {
    if (sessionID == "" || sessionID.length != 8) return false;
    return !isNaN(sessionID);
}



/* JavaScript für ein image-carousel im iPhone-Mockup*/
window.addEventListener("DOMContentLoaded", () => {
    const images = document.querySelectorAll(".image");
    const screen = document.querySelector(".screen");
    const TIME_BETWEEN_IMAGES = 6000;

    let imageIndex = 0;
/*Das Wechseln der Bilder pausiert, wenn man mit der Maus darauf geht*/
    screen.addEventListener("mouseenter", () => {
        clearInterval(imageInterval);
    });
/*Es geht weiter sobald man wieder mit der Maus runtergeht*/
    screen.addEventListener("mouseleave", () => {
        imageInterval = setInterval(imageChanger, TIME_BETWEEN_IMAGES);
    });

    let imageChanger = () => {
        if (imageIndex < 0 || imageIndex > images.length - 1) return;
        imageIndex = (imageIndex + 1) % images.length;
        images.forEach(it => it.classList.remove("active"));
        images[imageIndex].classList.add("active");
    }

    var imageInterval = setInterval(imageChanger, TIME_BETWEEN_IMAGES);
});
